# BancoMundial

O projeto está divido em 3 pastas:
APP - Onde está o frontend
BACKEND - Onde está o backend
CONECTOR - Backend que se conecta a API do Banco Mundial

Configurações para executar os projetos:
APP:
- npm install
- npm run build
  
O Comando acima irá gerar a pasta DIST onde o Dockerfile irá copiar os arquivos para a imagem

BACKEND:
- ./mvnw install quarkus:dev

O Comando acima irá rodar o teste e gerar o jar dentro da pasta TARGET.

CONECTOR:
- ./mvnw install quarkus:dev

O Comando acima irá rodar o teste e gerar o jar dentro da pasta TARGET.


Após isso basta executar o docker-compose up


No seu navegador basta acessar localhost
