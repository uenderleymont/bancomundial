package org.bancomundial.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ConsultaDTO {
    private IndicadorDTO indicator;
    private int date;
    private double value;
    private String countryiso3code;

    @JsonIgnore
    private String country;

    @JsonIgnore
    private String obs_status;

    @JsonIgnore
    private String decimal;

    @JsonIgnore
    private String unit;


    public IndicadorDTO getIndicator() {
        return this.indicator;
    }

    public void setIndicator(IndicadorDTO indicator) {
        this.indicator = indicator;
    }

    public int getDate() {
        return this.date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public double getValue() {
        return this.value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getCountryiso3code() {
        return this.countryiso3code;
    }

    public void setCountryiso3code(String countryiso3code) {
        this.countryiso3code = countryiso3code;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getObs_status() {
        return this.obs_status;
    }

    public void setObs_status(String obs_status) {
        this.obs_status = obs_status;
    }

    public String getDecimal() {
        return this.decimal;
    }

    public void setDecimal(String decimal) {
        this.decimal = decimal;
    }

    public String getUnit() {
        return this.unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

}
