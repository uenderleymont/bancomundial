package org.bancomundial.model;

import javax.json.bind.annotation.JsonbProperty;

public class IndicadorDTO {
    private String value;
    private String id;

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
