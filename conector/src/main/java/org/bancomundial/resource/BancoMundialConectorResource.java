package org.bancomundial.resource;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.bancomundial.model.ConsultaDTO;
import org.bancomundial.service.BancoMundialService;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.rest.client.inject.RestClient;

@Path("/conector")
public class BancoMundialConectorResource {

    private final Logger log = LoggerFactory.getLogger(BancoMundialConectorResource.class);

    @Inject
    @RestClient
    BancoMundialService bancoMundialService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Consulta o indicador de um determinado pais.")
    public Response consultar(@QueryParam(value = "codigoPais") String codigoPais) {
        log.info("Iniciando consulta na api do banco mundial");

        String json = bancoMundialService.consultarPais(codigoPais, "json");

        if(json == null) {
			return Response.serverError().build();
		} else {
            return Response.ok(converter(json)).build();
        }
    }

    public  List<ConsultaDTO> converter(String json){
        List<ConsultaDTO> consultasDTO = new ArrayList<ConsultaDTO>();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            JsonNode jsonNode = objectMapper.readTree(json);
            ArrayNode datasetArray = (ArrayNode) jsonNode.get(1);

            datasetArray.forEach(obj -> {
                try {
                    ConsultaDTO treeToValue = objectMapper.treeToValue(obj, ConsultaDTO.class);
                    consultasDTO.add(treeToValue);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            });
		} catch (JsonMappingException e1) {
			e1.printStackTrace();
		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
        }
        return consultasDTO;
    }
}
