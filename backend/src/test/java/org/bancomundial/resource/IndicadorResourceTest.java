package org.bancomundial.resource;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;

@QuarkusTest
public class IndicadorResourceTest {
    @Test
    public void testarSeStatusCodeE200(){
        RestAssured.given().queryParam("codigoPais", "br").get("api/indicador").then().statusCode(200);
    }
}
