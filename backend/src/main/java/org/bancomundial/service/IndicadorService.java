package org.bancomundial.service;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.bancomundial.model.DadoIndicadorDTO;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import org.jboss.resteasy.annotations.jaxrs.QueryParam;

@Path("/conector")
@RegisterRestClient(configKey = "bancomundial-service-key")
public interface IndicadorService {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<DadoIndicadorDTO> consultarPais(@QueryParam("codigoPais") String codigoPais);
}
