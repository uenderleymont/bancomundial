package org.bancomundial.resource;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.bancomundial.model.DadoIndicadorDTO;
import org.bancomundial.service.IndicadorService;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.rest.client.inject.RestClient;

@Path("/api")
public class IndicadorResource {

    private final Logger log = LoggerFactory.getLogger(IndicadorResource.class);

    @Inject
    @RestClient
    IndicadorService indicadorService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/indicador")
    @Operation(summary = "Consulta o indicador de um determinado pais.",
               description = "Irá consultar todas as cotações já consultadas e armazenadas anteriormente.")
    public Response indicador(@QueryParam(value = "codigoPais") String codigoPais) {
        Response response = null;
        log.info("Chamando servico de consulta de indicador");
        List<DadoIndicadorDTO> indicadores = indicadorService.consultarPais(codigoPais);

        if(indicadores == null){
            response = Response.status(Status.NOT_FOUND).build();
        } else {
            response = Response.ok(indicadores).build();
        }
        return response;
    }
}
