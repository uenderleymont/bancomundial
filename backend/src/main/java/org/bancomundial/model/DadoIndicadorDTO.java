package org.bancomundial.model;

public class DadoIndicadorDTO {
    private IndicadorDTO indicator;
    private int date;
    private double value;
    private String countryiso3code;

    public IndicadorDTO getIndicator() {
        return this.indicator;
    }

    public void setIndicator(IndicadorDTO indicator) {
        this.indicator = indicator;
    }

    public int getDate() {
        return this.date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public double getValue() {
        return this.value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getCountryiso3code() {
        return this.countryiso3code;
    }

    public void setCountryiso3code(String countryiso3code) {
        this.countryiso3code = countryiso3code;
    }
}
