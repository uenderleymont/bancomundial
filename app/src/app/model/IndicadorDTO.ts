export class IndicadorDTO {
  date: number;
  value: number;
  countryiso3code: string;
}
