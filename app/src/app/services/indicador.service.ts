import { IndicadorDTO } from './../model/IndicadorDTO';
import { Injectable } from '@angular/core';

import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IndicadorService {

  private indicadorUrl = 'http://localhost:8090/api';

  constructor(
    private http: HttpClient) { }

  getIndicadores (codigoPais: string): Observable<IndicadorDTO[]> {
    let params = new HttpParams().set('codigoPais', codigoPais);

    return this.http.get<IndicadorDTO[]>(this.indicadorUrl + '/indicador', { params: params});
  }
}
