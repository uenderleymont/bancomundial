import { IndicadorDTO } from './model/IndicadorDTO';
import { IndicadorService } from './services/indicador.service';
import {MatTableDataSource} from '@angular/material/table';

import { Component, OnInit } from '@angular/core';
import  {FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit {

  constructor(private indicadorService: IndicadorService) {}

  indicadores: IndicadorDTO[] = [];
  dataSource: IndicadorDTO[] = [];

  codigoPais = new FormControl('', [Validators.required]);
  loading = false;

  displayedColumns: string[] = ['date', 'value', 'countryiso3code'];

  ngOnInit(): void { }

  getErrorMessage() {
    if (this.codigoPais.hasError('required')) {
      return 'Favor preencher com algum valor';
    }
    return this.codigoPais.hasError('codigoPais') ? 'valor inválido' : '';
  }

  pesquisar() {
    this.loading = true;
    this.indicadorService.getIndicadores(this.codigoPais.value).subscribe(response => {
        this.indicadores = response;
        this.loading = false;
        this.dataSource = this.indicadores;
    }, (err) => {
      alert('Erro no consumo da API');
      this.loading = false;
    });

  }
}
