# BancoMundial

# Historia de Usuário
## H1 - Consultar dados de cotação na API do Banco Central
	Como usuário, quero consultar através da aplicação **banco_mundial** os indicadores de probreza de um determinado país.

	No formulário ao informar o código do país serão apresentados os dados em uma tabela. Os dados devem ser:
	- Data do indicador
	- Valor do indicador
	- País

## Informações
Os possíveis códigos de paises podem ser encontrados no link http://api.worldbank.org/v2/country
